#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
/*
struct OBB {
	std::vector<glm::vec3> Points;

	glm::vec3 P12= Points[2]- Points[1];
	glm::vec3 P23= Points[3]- Points[2];
	glm::vec3 P34= Points[4] - Points[3];
	glm::vec3 P41= Points[1] - Points[4];

	glm::vec3 getVector(int pos) {
		switch (pos){
			case 0:
				return P12;
				break;
			case 1:
				return P23;
				break;
			case 2:
				return P34;
				break;
			case 3:
				return P41;
				break;
		default:
			return glm::vec3(NULL,NULL, NULL);
			break;
		}
	}
};*/

struct AABB {
	glm::vec3 OCentre;
	glm::vec3 OExtent;
	glm::vec3 Centre;
	glm::vec3 Extent;
	glm::vec3 OMin;
	glm::vec3 OMax;
	glm::vec3 Min;
	glm::vec3 Max;
};

struct GameObject{
	int _objectType;
	glm::vec3 _translate;
	float _angle;
	glm::vec3 _rotation;
	glm::vec3 _scale;
	GLuint _textureID;
	std::string _textureFile;
	//OBB _boundingOBB;
	AABB _boundingAABB;
	int _materialType;
	bool isLight=false;
	bool isOn = false;

	bool OverlapAABB(AABB two);
	//bool OverlapOBB(OBB BB2);

	friend std::ostream& operator<<(std::ostream& os, const GameObject& p);

	friend std::ofstream& operator<<(std::ofstream& os, const GameObject& p);
	friend std::ifstream& operator>>(std::ifstream& is, GameObject& p);

};