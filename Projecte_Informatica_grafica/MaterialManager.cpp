#include "MaterialManager.h"

void MaterialManager::createMaterialDefinitions(){
	_materialData[ROCK].ambient = glm::vec3(0.05375f, 0.05f, 0.06625f);
	_materialData[ROCK].diffuse = glm::vec3(0.18275f, 0.17f, 0.22525f);
	_materialData[ROCK].specular = glm::vec3(0.332741f, 0.328634f, 0.346435f);
	_materialData[ROCK].shininess = 0.3f;
	_materialData[GOLD].ambient = glm::vec3(0.2475f, 0.1995f, 0.0745f);
	_materialData[GOLD].diffuse = glm::vec3(0.75164f, 0.60648f, 0.22648f);
	_materialData[GOLD].specular = glm::vec3(0.628281f, 0.555802f, 0.366065f);
	_materialData[GOLD].shininess = 0.4f;
	_materialData[METAL].ambient = glm::vec3(0.19125f, 0.0735f, 0.0225f);
	_materialData[METAL].diffuse = glm::vec3(0.7038f, 0.27048f, 0.0828f);
	_materialData[METAL].specular = glm::vec3(0.256777f, 0.137622f, 0.086014f);
	_materialData[METAL].shininess = 0.1f;
	_materialData[LLUM].ambient = glm::vec3(1.5f, 1.5f, 1.5f);
	_materialData[LLUM].diffuse = glm::vec3(1.7038f, 1.27048f, 1.0828f);
	_materialData[LLUM].specular = glm::vec3(0.256777f, 0.137622f, 0.086014f);
}

MaterialManager::MaterialManager(){
}

MaterialManager::~MaterialManager(){
}

material MaterialManager::getMaterialComponents(int materialID){
	return _materialData[materialID];
}

int MaterialManager::getMaterialID(std::string materialName){
	for (int i = 0; i < NUMMATERIALS; i++) {
		if (_materialType[i] == materialName) {
			return i;
		}
	}
	return -1;
}
