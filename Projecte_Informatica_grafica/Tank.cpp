#include "Tank.h"



Tank::Tank(){
}

void Tank::initTank(glm::vec3 positiontop, glm::vec3 positionbottom, float angleTOP, float angleBOTTOM, int health, int delay){
	_positionTOP = positiontop;
	_positionBOTTOM = positionbottom;
	_angleTOP = angleTOP;
	_angleBOTTOM = angleBOTTOM;
	_health = health;
	_delay = delay;
	Shot = false;
}

void Tank::moveTank(float x, float y){
	_positionTOP.x += x;
	_positionBOTTOM.x += x;
	_positionTOP.y += y;
	_positionBOTTOM.y += y;
}

/*
defineix el angle de la torreta
*/
void Tank::setAngleTOP(int angle){
	_angleTOP += angle;
}
/*
rota tot el tanc
*/
void Tank::rotateTank(int angle){
	_angleBOTTOM += angle;
	_angleTOP += angle;
}

glm::vec3 Tank::getPositionTOP(){
	return _positionTOP;
}

glm::vec3 Tank::getPositionBOTTOM(){
	return _positionBOTTOM;
}
/*
retorna angle torreta
*/
float Tank::getAngleTOP(){
	return _angleTOP;
}
/*
retorna angle general del tanc
*/
float Tank::getAngleBOTTOM(){
	return _angleBOTTOM;
}
/*
retorna el  angle de la bala
*/
float Tank::getAngleBala(){
	return AngleBala;
}
/*
funcio que inicia les variables per a disparar
*/
void Tank::Fire(){
	start = std::clock();
	_delay = 5000;
	Shot = true;
	AngleBala = _angleTOP;
}
/*
funcio que retorna si pot disparar
*/
bool Tank::canFire(){
	return std::clock() - start > _delay;
}

bool Tank::ShotActive(){
	if (Shot && canFire()) {
		Shot = false;
		return true;
	}
	return false;
}
/*
funcions per limitar el moviment del tanc quan estem en mode bala slow
*/
bool Tank::getCanMove(){
	return _canMove;
}

void Tank::setCanMove(bool estate){
	_canMove = estate;
	if (estate)_delay = 0;
}

void Tank::setAnglebala(int angle) {
	AngleBala += angle;
}

Tank::~Tank(){
}
