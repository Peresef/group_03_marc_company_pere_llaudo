#pragma once
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include <ctime>

class Tank
{
private:
	glm::vec3 _positionTOP;
	glm::vec3 _positionBOTTOM;
	float _angleTOP;
	float _angleBOTTOM;
	int _health;
	int _delay;
	bool Shot;
	bool _canMove=true;
	float AngleBala;
	std::clock_t start; ///per fer calculs de timing

public:
	Tank();
	void initTank(glm::vec3 positiontop, glm::vec3 positionbottom, float angleTOP, float angleBOTTOM, int health, int delay);
	void moveTank(float x, float y);
	void setAngleTOP(int angle);
	void rotateTank(int angle);
	glm::vec3 getPositionTOP();
	glm::vec3 getPositionBOTTOM();
	float getAngleTOP();
	float getAngleBOTTOM();
	float getAngleBala();
	void Fire();
	bool canFire();
	bool ShotActive();
	bool getCanMove();
	void setCanMove(bool estate);
	void setAnglebala(int angle);
	~Tank();
};

