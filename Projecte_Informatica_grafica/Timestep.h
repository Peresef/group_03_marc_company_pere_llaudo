#pragma once
#include <ctime>
#include "Time.h"
#include <windows.h>
#include <windowsx.h>
#include <iostream>

class Timestep{
private:
	State current;
	State previous;
	//float t;
	float dt;
	float currentTime;
	float accumulator;
	std::clock_t t;

public:
	Timestep();
	~Timestep();
	State interpolate(const State &previous, const State &current, float alpha);
	void SemiFixedTimeStep();
	void FixedDeltaTime();
	float acceleration(const State &state, float t);
	Derivative evaluate(const State &initial, float t);
	Derivative evaluate(const State &initial, float t, float dt, const Derivative &d);
	void integrate(State &state, float t, float dt);
	float time();
};






