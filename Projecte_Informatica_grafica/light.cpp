#include "Light.h"

Light::Light(){
}

Light::Light(glm::vec3 position, glm::vec3 direction, glm::vec3 ambient,
	glm::vec3 diffuse, glm::vec3 specular, int type) :
	_position(position),
	_direction(direction),
	_ambient(ambient),
	_diffuse(diffuse),
	_specular(specular),
	_type(type){
}

Light::~Light(){
}
/*
no crec que facin falta comentaris, son getters i setters a tutiplen
*/
glm::vec3 Light::getPosition(){
	return _position;
}

void Light::setPosition(glm::vec3 position){
	_position = position;
}

glm::vec3 Light::getDirection(){
	return _direction;
}

void Light::setDirection(glm::vec3 direction){
	_direction = direction;
}

glm::vec3 Light::getAmbient(){
	return _ambient;
}

void Light::setAmbient(glm::vec3 ambient){
	_ambient = ambient;
}

glm::vec3 Light::getDiffuse(){
	return _diffuse;
}

void Light::setDiffuse(glm::vec3 diffuse){
	_diffuse = diffuse;
}

glm::vec3 Light::getSpecular(){
	return _specular;
}

void Light::setSpecular(glm::vec3 specular){
	_specular = specular;
}

int Light::getType(){
	return _type;
}

void Light::setType(int type){
	_type = type;
}

float Light::getCutOff(){
	return _cutOff;
}

void Light::setCutOff(float cutOff){
	_cutOff = cutOff;
}

float Light::getConstant(){
	return _constant;
}

void Light::setConstant(float constant){
	_constant = constant;
}

float Light::getLinear(){
	return _linear;
}

void Light::setLinear(float linear){
	_linear = linear;
}

float Light::getQuadratic(){
	return _quadratic;
}

void Light::setQuadratic(float quadratic){
	_quadratic = quadratic;
}

bool Light::getIsOn(){
	return _isOn;
}

void Light::setIsOn(bool isOn){
	_isOn = isOn;
}
