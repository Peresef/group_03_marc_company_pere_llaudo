#pragma once
#include <glm/glm.hpp>

class Light{
private:
	glm::vec3 _position;
	glm::vec3 _direction;

	glm::vec3 _ambient;
	glm::vec3 _diffuse;
	glm::vec3 _specular;

	int _type;

	float _cutOff;

	float _constant;
	float _linear;
	float _quadratic;

	bool _isOn;

public:
	Light();
	Light(glm::vec3 position, glm::vec3 direction, glm::vec3 ambient,
		glm::vec3 diffuse, glm::vec3 specular, int type);
	~Light();

	glm::vec3 getPosition();
	void setPosition(glm::vec3 position);

	glm::vec3 getDirection();
	void setDirection(glm::vec3 direction);

	glm::vec3 getAmbient();
	void setAmbient(glm::vec3 ambient);
	glm::vec3 getDiffuse();
	void setDiffuse(glm::vec3 diffuse);
	glm::vec3 getSpecular();
	void setSpecular(glm::vec3 specular);

	int getType();
	void setType(int type);

	float getCutOff();
	void setCutOff(float cutOff);

	float getConstant();
	void setConstant(float constant);
	float getLinear();
	void setLinear(float linear);
	float getQuadratic();
	void setQuadratic(float quadratic);

	bool getIsOn();
	void setIsOn(bool isOn);
};

